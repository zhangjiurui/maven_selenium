package ToolsQA.DemoMaven;

import org.openqa.selenium.WebDriver;		
//import org.openqa.selenium.firefox.FirefoxDriver;	
import org.openqa.selenium.chrome.*;
import org.testng.Assert;		

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class FirstMavenTC {
	
	private WebDriver driver;	
	
@Test(groups = {"SmokeTest", "RegTest"})

	  public void testEasy() {
	 
	driver.get("http://www.guru99.com/selenium-tutorial.html");  
		String title = driver.getTitle();				 
		Assert.assertTrue(title.contains("Free Selenium Tutorials")); 	
		
		 System.out.println("It is successful for Test3");
}
@BeforeTest(groups = "SmokeTest")
public void beforeTest() {
	  
	  String exePath = "C:\\Java Selenium\\drivers\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);

	 // driver = new FirefoxDriver();  
	  driver = new ChromeDriver(); 

}

@AfterTest(groups = "SmokeTest")
public void afterTest() {
	  driver.quit();	
}

}

